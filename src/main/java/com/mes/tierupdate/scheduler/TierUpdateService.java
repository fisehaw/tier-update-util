package com.mes.tierupdate.scheduler;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mes.tierupdate.model.MerchantTier;
import com.mes.tierupdate.repository.TierUpdateRepositoryImpl;

@Component
public class TierUpdateService {

	private static final Logger log = LoggerFactory.getLogger(TierUpdateService.class);

	@Autowired
	TierUpdateRepositoryImpl tierUpdateRepository;

    @Scheduled(cron = "${cron.expression}")
	public void scheduleTaskWithCronExpression() {
		log.info("Cron Task :: starting");
        List<MerchantTier> merchantList = (List<MerchantTier>) tierUpdateRepository.findAllMerchantTiers();
        tierUpdateRepository.save(merchantList);
        log.info("Cron Task :: completed");
	}
}
