package com.mes.tierupdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan
public class TierUpdateUtilApplication {

	private static final Logger log = LoggerFactory.getLogger(TierUpdateUtilApplication.class);
	
	public static void main(String[] args) {
		log.info("Starting TierUpdateUtilApplication");
		SpringApplication.run(TierUpdateUtilApplication.class, args);
		
		log.info("Application started.");
	}

}
