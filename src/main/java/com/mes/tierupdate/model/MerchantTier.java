package com.mes.tierupdate.model;

import javax.persistence.Entity;


public class MerchantTier {
	
	private Integer tierLevel;	
	private Long merchantNumber;

	public Integer getTierLevel() {
		return tierLevel;
	}

	public void setTierLevel(Integer tierLevel) {
		this.tierLevel = tierLevel;
	}

	public Long getMerchantNumber() {
		return merchantNumber;
	}

	public void setMerchantNumber(Long merchantNumber) {
		this.merchantNumber = merchantNumber;
	}

}
