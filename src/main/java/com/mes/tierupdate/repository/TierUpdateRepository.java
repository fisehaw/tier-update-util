package com.mes.tierupdate.repository;

import java.util.List;
import com.mes.tierupdate.model.MerchantTier;

public interface TierUpdateRepository {

	public List<MerchantTier> findAllMerchantTiers();

	public void save(List<MerchantTier> merchantList);

}
