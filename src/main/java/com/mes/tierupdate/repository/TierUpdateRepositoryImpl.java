package com.mes.tierupdate.repository;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.mes.tierupdate.mapper.TierUpdateUtilMapper;
import com.mes.tierupdate.model.MerchantTier;

@Repository
public class TierUpdateRepositoryImpl implements TierUpdateRepository {
	private static final Logger log = LoggerFactory.getLogger(TierUpdateRepositoryImpl.class);

	private static final String SQL_ALL_MERCHANT_TIER = "WITH mid_roll_12 AS " +
			"  (SELECT s.merchant_number , " +
			"    SUM( nvl(s.vital_merchant_income,cast(0.0 as float)) + nvl(s.fee_dce_adj_amount,cast(0.0 as float)) + nvl(s.disc_ic_dce_adj_amount,cast(0.0 as float)) - nvl(s.debit_network_fees,cast(0.0 as float)) - ( nvl(s.interchange_expense,cast(0.0 as float)) - nvl(s.interchange_expense_enhanced,nvl(s.interchange_expense,cast(0.0 as float))) ) - nvl(s.interchange_expense_enhanced,nvl(s.interchange_expense,cast(0.0 as float))) - ( nvl(s.vmc_assessment_expense,cast(0.0 as float)) - nvl(s.vmc_fees,cast(0.0 as float)) ) - nvl(s.vmc_fees,cast(0.0 as float)) - nvl(s.tot_partnership,cast(0.0 as float)) ) as Total_Net_Contribution_Mes " +
			"  FROM monthly_extract_summary s " +
			"  WHERE " +
			"    /*--last 12 months*/ " +
			"    add_months(active_date, 11) >= " +
			"    (SELECT max(active_date) FROM monthly_extract_summary ) " +
			"  GROUP BY merchant_number ) " +
			"SELECT " +
			"  /*format for upload mid + comma + tier*/ " +
			"  merchant_number , " +
			"  case " +
			"    when nvl(Total_Net_Contribution_Mes,0) < 501 then 1 " +
			"    when Total_Net_Contribution_Mes < 1001       then 2 " +
			"    when Total_Net_Contribution_Mes < 2501       then 3 " +
			"    when Total_Net_Contribution_Mes < 5001       then 4 " +
			"    when Total_Net_Contribution_Mes < 10001      then 5 " +
			"    when Total_Net_Contribution_Mes < 25001      then 6 " +
			"    when Total_Net_Contribution_Mes < 100001     then 7 " +
			"    else 8 " +
			"  end tier_level " +
			"FROM mid_roll_12 " ;
	
	private static final String SQL_UPDATE_TIER_LEVEL = "update merchant set tier_x_code = ? where merch_number = ?" ;
	
	@Autowired
	JdbcTemplate jdbcTemplate;;

	@Override
	public List<MerchantTier> findAllMerchantTiers() {
		List<MerchantTier> merchantTierList = new ArrayList<MerchantTier>();
		try {
			merchantTierList = jdbcTemplate.query(SQL_ALL_MERCHANT_TIER, new TierUpdateUtilMapper());
		}catch (Exception e) {
			log.error("Failed to get list of merchant tiers" ,e);			
		}		
		return merchantTierList;
	}

	@Override
	public void save(List<MerchantTier> merchantList) {
		for (MerchantTier merchanTier : merchantList) {
			try {
				jdbcTemplate.update(SQL_UPDATE_TIER_LEVEL, merchanTier.getTierLevel(),merchanTier.getMerchantNumber());
			}catch (Exception e) {
				log.error("Failed to update merchant tier" ,e);
				
			}	        
		}	
	}
}
