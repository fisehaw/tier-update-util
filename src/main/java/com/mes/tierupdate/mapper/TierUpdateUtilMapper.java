package com.mes.tierupdate.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.mes.tierupdate.model.MerchantTier;

public class TierUpdateUtilMapper implements RowMapper<MerchantTier > {

	@Override
	public MerchantTier mapRow(ResultSet rs, int rowNum) throws SQLException {
		MerchantTier dto = new MerchantTier();
		dto.setMerchantNumber(rs.getLong("merchant_number"));
		dto.setTierLevel(rs.getInt("tier_level"));
		return dto;
	}

}
